#!/usr/bin/env python

from setuptools import setup, Extension
import pkgconfig

assert pkgconfig.installed('lcfg-packages','>1.0')
lcfg_packages = pkgconfig.parse('lcfg-packages')

setup(name='lcfg',
      setup_requires=['nose>=1.0',
                      'pkgconfig',],
      version='0.0',
      description='Python Bindings to the lcfg-core library',
      author='Magnus Hagdorn',
      author_email='magnus.hagdorn@ed.ac.uk',
      url='https://www.lcfg.org',
      packages=['lcfg'],
      ext_modules=[Extension('lcfg._packages',['lcfg/packages.i'],
                             libraries=list(lcfg_packages['libraries']),
                             include_dirs=list(lcfg_packages['include_dirs']),
                             library_dirs=list(lcfg_packages['library_dirs']),
                             swig_opts=['-modern']),
                   Extension('lcfg._common',['lcfg/common.i'],
                             libraries=list(lcfg_packages['libraries']),
                             include_dirs=list(lcfg_packages['include_dirs']),
                             library_dirs=list(lcfg_packages['library_dirs']),
                             swig_opts=['-modern']),
      ],
     )
