%module packages
%{
  #include <lcfg/packages.h>
%}

%include error.i

/************************************************************************
 *   the LCFGPackage object                                             *
 ************************************************************************/
%feature("ref")   LCFGPackage "lcfgpackage_acquire($this);"
%feature("unref") LCFGPackage "lcfgpackage_relinquish($this);"

%rename(valid_name) lcfgpackage_valid_name;
bool lcfgpackage_valid_name(char * name);
%rename(valid_arch) lcfgpackage_valid_arch;
bool lcfgpackage_valid_arch(char * arch);
%rename(valid_version) lcfgpackage_valid_version;
bool lcfgpackage_valid_version(char * version);
%rename(valid_release) lcfgpackage_valid_release;
bool lcfgpackage_valid_release(char * release);
%rename(valid_prefix) lcfgpackage_valid_prefix;
bool lcfgpackage_valid_prefix(char prefix);
%rename(valid_flag_chr) lcfgpackage_valid_flag_chr;
bool lcfgpackage_valid_flag_chr(char flag);
%rename(valid_flags) lcfgpackage_valid_flags;
bool lcfgpackage_valid_flags(char * flags);
%rename(valid_category) lcfgpackage_valid_category;
bool lcfgpackage_valid_category(char * category);

%inline %{
  /* wrapper for lcfgpackage_to_string that returns a string */
  const char * lcfgpackage_to_string_wrapper( const LCFGPackage * pkg,
					      const char * defarch,
					      LCFGPkgStyle style,
					      LCFGOption options) {
    char * result;
    size_t size;
    size = 0;
    result = NULL;
    lcfgpackage_to_string(pkg, defarch, style, options, &result, &size);
    return result;
  }
    
%}

struct LCFGPackage {
  %extend {
    LCFGPackage() {
      return lcfgpackage_new();
    }
    LCFGPackage(const char *spec) {
      LCFGStatus status;
      char *msg;
      LCFGPackage *package;

      status = lcfgpackage_from_spec(spec, &package, &msg);
      
      if (status != LCFG_STATUS_OK) {
	throw_exception(status,msg);
	return NULL;
      }
      else
	return package;
    }
    ~LCFGPackage() {
      lcfgpackage_relinquish($self);
    }
    LCFGPackage * __copy__() {
      return lcfgpackage_clone($self);
    }
    
    const bool is_valid;
    
    const bool has_name;
    char *name;

    const bool has_arch;
    char *arch;

    const bool has_version;
    char *version;    

    const bool has_release;
    char *release;    

    const bool has_prefix;
    char prefix;
    void clear_prefix() {
      if (!lcfgpackage_clear_prefix($self))
	throw_exception(LCFG_STATUS_ERROR,"clearing prefix");
    }

    const bool has_flags;
    char *flags;
    void clear_flags() {
      if (!lcfgpackage_clear_flags($self))
	throw_exception(LCFG_STATUS_ERROR,"clearing flags");
    }
    bool has_flag(char flag) {
      return lcfgpackage_has_flag($self,flag);
    }
    void add_flags(char *flags) {
      if (!lcfgpackage_add_flags($self,flags))
	throw_exception(LCFG_STATUS_ERROR,"adding flags");
    }

    const bool has_category;
    char *category;

    %immutable;
    const char * full_version;
    %immutable;
    const char * id;

    bool match(const char *name, const char *version) {
      return lcfgpackage_match($self,name,version);
    }

    int compare_version(LCFGPackage *other) {
      return lcfgpackage_compare_versions($self, other);
    }
    int compare_name(LCFGPackage *other) {
      return lcfgpackage_compare_names($self, other);
    }
    int compare_arch(LCFGPackage *other) {
      return lcfgpackage_compare_archs($self, other);
    }
    int compare(LCFGPackage *other) {
      return lcfgpackage_compare($self, other);
    }
    int __eq__(LCFGPackage *other) {
      return lcfgpackage_equals($self, other);
    }
    bool same_context(LCFGPackage *other) {
      return lcfgpackage_same_context($self, other);
    }
    unsigned long __hash__() {
      return lcfgpackage_hash($self);
    }

    const char * __repr__() {
      return lcfgpackage_to_string_wrapper($self, NULL, LCFG_PKG_STYLE_SPEC, LCFG_OPT_ALL_VALUES);
    }

    const char * to_spec(char * arch=NULL, int options = 0) {
      return lcfgpackage_to_string_wrapper($self, arch, LCFG_PKG_STYLE_SPEC, options);
    }
    const char * to_rpm(char * arch=NULL, int options = 0) {
      return lcfgpackage_to_string_wrapper($self, arch, LCFG_PKG_STYLE_RPM, options);
    }
    const char * to_cpp(char * arch=NULL, int options = 0) {
      return lcfgpackage_to_string_wrapper($self, arch, LCFG_PKG_STYLE_CPP, options);
    }
    const char * to_xml(char * arch=NULL, int options = 0) {
      return lcfgpackage_to_string_wrapper($self, arch, LCFG_PKG_STYLE_XML, options);
    }
    const char * to_summary(char * arch=NULL, int options = 0) {
      return lcfgpackage_to_string_wrapper($self, arch, LCFG_PKG_STYLE_SUMMARY, options);
    }
    const char * to_eval(char * arch=NULL, int options = 0) {
      return lcfgpackage_to_string_wrapper($self, arch, LCFG_PKG_STYLE_EVAL, options);
    }
  }
};

%{
  const bool LCFGPackage_is_valid_get(LCFGPackage *p) {
    return lcfgpackage_is_valid(p);
  }

  /* name */
  const bool LCFGPackage_has_name_get(LCFGPackage *p) {
    return lcfgpackage_has_name(p);
  }
  const char * LCFGPackage_name_get(LCFGPackage *p) {
    return lcfgpackage_get_name(p);
  }
  void LCFGPackage_name_set(LCFGPackage *p, char *name) {
    if (!lcfgpackage_set_name(p, strdup(name)))
      throw_exception(LCFG_STATUS_ERROR,"setting name");
  }

  /* arch */
  const bool LCFGPackage_has_arch_get(LCFGPackage *p) {
    return lcfgpackage_has_arch(p);
  }
  const char * LCFGPackage_arch_get(LCFGPackage *p) {
    return lcfgpackage_get_arch(p);
  }
  void LCFGPackage_arch_set(LCFGPackage *p, char *arch) {
    if (!lcfgpackage_set_arch(p, strdup(arch)))
      throw_exception(LCFG_STATUS_ERROR,"setting arch");
  }

  /* version */
  const bool LCFGPackage_has_version_get(LCFGPackage *p) {
    return lcfgpackage_has_version(p);
  }
  const char * LCFGPackage_version_get(LCFGPackage *p) {
    return lcfgpackage_get_version(p);
  }
  void LCFGPackage_version_set(LCFGPackage *p, char *version) {
    if (!lcfgpackage_set_version(p, strdup(version)))
      throw_exception(LCFG_STATUS_ERROR,"setting version");
  }

  /* release */
  const bool LCFGPackage_has_release_get(LCFGPackage *p) {
    return lcfgpackage_has_release(p);
  }
  const char * LCFGPackage_release_get(LCFGPackage *p) {
    return lcfgpackage_get_release(p);
  }
  void LCFGPackage_release_set(LCFGPackage *p, char *release) {
    if (!lcfgpackage_set_release(p, strdup(release)))
      throw_exception(LCFG_STATUS_ERROR,"setting release");
  }

  /* prefix */
  const bool LCFGPackage_has_prefix_get(LCFGPackage *p) {
    return lcfgpackage_has_prefix(p);
  }
  const char LCFGPackage_prefix_get(LCFGPackage *p) {
    return lcfgpackage_get_prefix(p);
  }
  void LCFGPackage_prefix_set(LCFGPackage *p, char prefix) {
    if (!lcfgpackage_set_prefix(p, prefix))
      throw_exception(LCFG_STATUS_ERROR,"setting prefix");
  }
  
  /* flags */
  const bool LCFGPackage_has_flags_get(LCFGPackage *p) {
    return lcfgpackage_has_flags(p);
  }
  const char * LCFGPackage_flags_get(LCFGPackage *p) {
    return lcfgpackage_get_flags(p);
  }
  void LCFGPackage_flags_set(LCFGPackage *p, char *flags) {
    if (!lcfgpackage_set_flags(p, strdup(flags)))
      throw_exception(LCFG_STATUS_ERROR,"setting flags");
  }

  /* category */
  const bool LCFGPackage_has_category_get(LCFGPackage *p) {
    return lcfgpackage_has_category(p);
  }
  const char * LCFGPackage_category_get(LCFGPackage *p) {
    return lcfgpackage_get_category(p);
  }
  void LCFGPackage_category_set(LCFGPackage *p, char *category) {
    if  (!lcfgpackage_set_category(p, strdup(category)))
      throw_exception(LCFG_STATUS_ERROR,"setting category");
  }


  /* further accessors and methods */
  const char *LCFGPackage_full_version_get(LCFGPackage *p) {
    return lcfgpackage_full_version(p);
  }
  const char *LCFGPackage_id_get(LCFGPackage *p) {
    return lcfgpackage_id(p);
  }
%}

/************************************************************************
 *   the LCFGPackageList object                                         *
 ************************************************************************/
%typemap(in) (char *key_name, char *key_arch) {
  char *name;
  char *arch;
  if (PyTuple_Check($input)) {
    if (!PyArg_ParseTuple($input,"ss",&name,&arch)) {
      PyErr_SetString(PyExc_TypeError,"tuple must contain 2 strings");
      return NULL;
    }    
    $1 = name;
    $2 = arch;
  }
  else {
    PyErr_SetString(PyExc_TypeError,"expected a tuple");
    return NULL;
  }
}

%feature("ref")   LCFGPackageList "lcfgpkglist_acquire($this);"
%feature("unref") LCFGPackageList "lcfgpkglist_relinquish($this);"

struct LCFGPackageList {
  %extend {
    LCFGPackageList() {
      return lcfgpkglist_new();
    }
    ~LCFGPackageList() {
      lcfgpkglist_relinquish($self);
    }

    int merge_rules;

    unsigned int __len__() {
      return lcfgpkglist_size($self);
    }

    void add(LCFGPackage *p) {
      LCFGChange change;
      char *msg;

      change = lcfgpkglist_merge_package($self, p, &msg);
      if (change == LCFG_CHANGE_ERROR)
	throw_exception(LCFG_STATUS_ERROR,msg);
    }
    void update(LCFGPackageList *other) {
      LCFGChange change;
      char *msg;

      change = lcfgpkglist_merge_list($self, other, &msg);
      if (change == LCFG_CHANGE_ERROR)
	throw_exception(LCFG_STATUS_ERROR,msg);
    }
    
    bool __contains__(char *key_name, char *key_arch) {
      return lcfgpkglist_has_package($self,key_name,key_arch);
    }
    LCFGPackage * __getitem__(char *key_name, char *key_arch) {
      if (!lcfgpkglist_has_package($self,key_name,key_arch)) {
	throw_package_key_exception(key_name,key_arch);
	return NULL;
      }
      else
	return lcfgpkglist_find_package($self,key_name,key_arch);
    }
    LCFGPackageIterator * __iter__() {
      return lcfgpkgiter_new($self);
    }
  }
};
    
%{
  int LCFGPackageList_merge_rules_get(LCFGPackageList *p) {
    return lcfgpkglist_get_merge_rules(p);
  }
  void LCFGPackageList_merge_rules_set(LCFGPackageList *p, int new_rules) {
    if (!lcfgpkglist_set_merge_rules(p, new_rules))
      throw_exception(LCFG_STATUS_ERROR,"setting merge rules");
  }
%}

/************************************************************************
 *   the LCFGPackageIterator object                                     *
 ************************************************************************/
struct LCFGPackageIterator {
  %extend {
    LCFGPackageIterator(LCFGPackageList *list) {
      return lcfgpkgiter_new(list);
    }
    ~LCFGPackageIterator() {
      lcfgpkgiter_destroy($self);
    }
    LCFGPackageIterator * __iter__() {
      return $self;
    }
    LCFGPackage * __next__() {
      LCFGPackage *p;
      p = lcfgpkgiter_next($self);
      if (p==NULL)
	throw_stop_iteration();
      return p;
    }
  }
};
