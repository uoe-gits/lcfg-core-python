%{
  #include <lcfg/common.h>
  #define LCFG_STATUS_KEY_ERROR 100
  #define LCFG_STOP_ITERATION   101
  
  static char error_message[256];
  int status;

  void throw_exception(LCFGStatus s, char *msg) {
    strncpy(error_message,msg,256);
    status = s;
  }

  void throw_package_key_exception(char *name, char *arch) {
    snprintf(error_message,256,"('%s', '%s')",name,arch);
    status = LCFG_STATUS_KEY_ERROR;
  }

  void throw_stop_iteration() {
    status = LCFG_STOP_ITERATION;
  }
  
  void clear_exception() {
    status = LCFG_STATUS_OK;
    error_message[0] = '\0';
  }
%}

%exception {
  clear_exception();
  $function
#if defined(SWIGPYTHON)
  if (status == LCFG_STATUS_WARN) {
    PyErr_SetString(PyExc_Warning, error_message);
    return NULL;
  }
  else if (status == LCFG_STATUS_ERROR) {
    PyErr_SetString(PyExc_RuntimeError, error_message);
    return NULL;
  }
  else if (status == LCFG_STATUS_KEY_ERROR) {
    PyErr_SetString(PyExc_KeyError,error_message);
    return NULL;
  }
  else if (status == LCFG_STOP_ITERATION) {
    PyErr_SetString(PyExc_StopIteration, "End of iterator");
    return NULL;
  }
#endif
}
