import lcfg.packages
import lcfg.common
import copy
import nose
from nose.tools import assert_equals, assert_true, assert_false, assert_raises

def test_create_package():
    a = lcfg.packages.LCFGPackage()

def check_create_package_from_spec_ok(spec):
    a = lcfg.packages.LCFGPackage(spec)
    assert_true(a.is_valid)
def check_create_package_from_spec_nok(spec):
    assert_raises(RuntimeError,lcfg.packages.LCFGPackage,spec)
def test_create_package_from_spec():
    for s in ['hello-1-2','hello-*-*','hello-*-*/noarch','noarch/hello-*-*','hello-*-*:a','+noarch/hello-*-*']:
        yield (check_create_package_from_spec_ok,s)
def test_create_package_from_spec_false():
    for s in ['hello','noarch/hello','+noarch/hello']:
        yield (check_create_package_from_spec_nok,s)
    
def test_copy():
    a = lcfg.packages.LCFGPackage()
    b = copy.copy(a)


def test_isvalid():
    a = lcfg.packages.LCFGPackage()
    assert_false(a.is_valid)
    
def test_valid_name_false():
    assert_false(lcfg.packages.valid_name('_'))
def check_valid_name_true(name):
    assert_true(lcfg.packages.valid_name(name))
def test_valid_name_true():
    for n in ['hello','hello_world','hello-world']:
        yield (check_valid_name_true,n)
def test_hasname_false():
    a = lcfg.packages.LCFGPackage()
    assert_false(a.has_name)
def test_name():
    a = lcfg.packages.LCFGPackage()
    name = 'hello_world'
    a.name = name
    assert_true(a.has_name)
    assert_equals(a.name,name)

def test_valid_arch_false():
    assert_false(lcfg.packages.valid_arch('*'))
def test_valid_arch_true():
    assert_true(lcfg.packages.valid_arch('i386'))
def test_hasarch_false():
    a = lcfg.packages.LCFGPackage()
    assert_false(a.has_arch)
def test_arch():
    a = lcfg.packages.LCFGPackage()
    arch = 'hello_world'
    a.arch = arch
    assert_true(a.has_arch)
    assert_equals(a.arch,arch)

def test_valid_version_false():
    assert_false(lcfg.packages.valid_version('-'))
def test_valid_version_true():
    assert_true(lcfg.packages.valid_version('10'))
def test_hasversion_false():
    a = lcfg.packages.LCFGPackage()
    assert_false(a.has_version)
def test_version():
    a = lcfg.packages.LCFGPackage()
    version = '10'
    a.version = version
    assert_true(a.has_version)
    assert_equals(a.version,version)
    
def test_valid_release_false():
    assert_false(lcfg.packages.valid_release('-'))
def test_valid_release_true():
    assert_true(lcfg.packages.valid_release('10'))
def test_hasrelease_false():
    a = lcfg.packages.LCFGPackage()
    assert_false(a.has_release)
def test_release():
    a = lcfg.packages.LCFGPackage()
    release = '10'
    a.release = release
    assert_true(a.has_release)
    assert_equals(a.release,release)
    
VALID_PREFIX="+=?~>"
def check_valid_prefix_false(prefix):
    assert_false(lcfg.packages.valid_prefix(prefix))
def check_valid_prefix_true(prefix):
    assert_true(lcfg.packages.valid_prefix(prefix))
def test_valid_prefix_false():
    for p in "a1X":
        yield (check_valid_prefix_false,p)
def test_valid_prefix_true():
    for p in VALID_PREFIX:
        yield (check_valid_prefix_true,p)
def test_hasprefix_false():
    a = lcfg.packages.LCFGPackage()
    assert_false(a.has_prefix)
def test_prefix():
    a = lcfg.packages.LCFGPackage()
    prefix = '+'
    a.prefix = prefix
    assert_true(a.has_prefix)
    assert_equals(a.prefix,prefix)
def test_clear_prefix():
    a = lcfg.packages.LCFGPackage()
    a.prefix = '+'
    assert_true(a.has_prefix)
    a.clear_prefix()
    assert_false(a.has_prefix)

def check_valid_flag_chr_false(f):
    assert_false(lcfg.packages.valid_flag_chr(f))
def check_valid_flag_chr_true(f):
    assert_true(lcfg.packages.valid_flag_chr(f))
def test_valid_flag_chr_false():
    for f in "+-@":
        yield (check_valid_flag_chr_false,f)
def test_valid_flag_chr_true():
    for f in "aB1":
        yield (check_valid_flag_chr_true,f)
def test_valid_flags_false():
    assert_false(lcfg.packages.valid_flags("ab+"))
def test_valid_flags_true():
    assert_true(lcfg.packages.valid_flags("ab1C"))
def test_flags():
    a = lcfg.packages.LCFGPackage()
    flags = 'Ab1'
    a.flags = flags
    assert_true(a.has_flags)
    assert_equals(a.flags,flags)
def test_clear_flags():
    a = lcfg.packages.LCFGPackage()
    a.flags = 'Ab1'
    assert_true(a.has_flags)
    a.clear_flags()
    assert_false(a.has_flags)
def test_has_flag():
    a = lcfg.packages.LCFGPackage()
    a.flags = 'Ab1'
    assert_true(a.has_flag,'A')
def test_has_flag_false():
    raise nose.SkipTest
    a = lcfg.packages.LCFGPackage()
    a.flags = 'Ab1'
    assert_false(a.has_flag,'2')
def test_add_flags():
    a = lcfg.packages.LCFGPackage()
    a.flags = 'Ab1'
    a.add_flags('cD2')
    assert_true(a.has_flag,'A')
    assert_true(a.has_flag,'c')
    assert_true(a.has_flag,'D')
    assert_true(a.has_flag,'2')
def test_add_flag():
    a = lcfg.packages.LCFGPackage()
    a.flags = 'Ab1'
    a.add_flags('c')
    assert_true(a.has_flag,'A')
    assert_true(a.has_flag,'c')

def test_valid_category_false():
    assert_false(lcfg.packages.valid_category('x y'))
def test_valid_category_true():
    assert_true(lcfg.packages.valid_category('xx'))
def test_hascategory_false():
    a = lcfg.packages.LCFGPackage()
    assert_false(a.has_category)
def test_category():
    a = lcfg.packages.LCFGPackage()
    category = 'xx'
    a.category = category
    assert_true(a.has_category)
    assert_equals(a.category,category)

def test_full_version_default():
    a = lcfg.packages.LCFGPackage()
    assert_equals(a.full_version,'*-*')
def test_full_version_noreel():
    a = lcfg.packages.LCFGPackage()
    a.version = '1.0'
    assert_equals(a.full_version,'1.0-*')
def test_full_version_nover():
    a = lcfg.packages.LCFGPackage()
    a.release = '1b'
    assert_equals(a.full_version,'*-1b')

class TestPackage:

    @classmethod
    def setup_class(cls):
        cls.a = lcfg.packages.LCFGPackage('hello-1.0-1b/arm')
        cls.b = lcfg.packages.LCFGPackage('world-2.0-10/i386')

        cls.c = copy.copy(cls.a)

    def test_full_version(self):
        assert_equals(self.a.full_version,'1.0-1b')

    def test_id(self):
        assert_equals(self.a.id,'hello.arm')
        
    def test_match_false(self):
        assert_false(self.a.match('hello','i386'))

    def test_match_true(self):
        assert_true(self.a.match('hello','arm'))

    def test_eq_true(self):
        assert_true(self.a == self.c)
    def test_neq(self):
        assert_true(self.a != self.b)
    def test_eq_false(self):
        assert_false(self.a == self.b)

    def test_compare_version_nok(self):
        assert_true(self.a.compare_version(self.b)<0)
    def test_compare_version_ok(self):
        assert_true(self.a.compare_version(self.c) == 0)

    def test_compare_name_nok(self):
        assert_true(self.a.compare_name(self.b)<0)
    def test_compare_name_ok(self):
        assert_true(self.a.compare_name(self.c) == 0)

    def test_compare_arch_nok(self):
        assert_true(self.a.compare_arch(self.b)<0)
    def test_compare_arch_ok(self):
        assert_true(self.a.compare_arch(self.c) == 0)

    def test_compare_nok(self):
        assert_true(self.a.compare(self.b)<0)
    def test_compare_ok(self):
        assert_true(self.a.compare(self.c) == 0)

    def test_same_context(self):
        assert_true(self.a.same_context(self.b))
        
    def test_hash(self):
        assert_equals(hash(self.a),7572451889889721)

    def test_repr(self):
        assert_equals(str(self.a),'hello-1.0-1b/arm')

class TestPackageString:
    @classmethod
    def setup_class(cls):
        cls.a = lcfg.packages.LCFGPackage('+noarch/hello-1.0-1b:b')

    def test_to_spec(self):
        assert_equals(self.a.to_spec(),'+hello-1.0-1b/noarch:b')
    def test_to_spec_noprefix(self):
        assert_equals(self.a.to_spec(options=lcfg.common.LCFG_OPT_NOPREFIX),'hello-1.0-1b/noarch:b')
    def test_to_rpm(self):
        assert_equals(self.a.to_rpm(),'hello-1.0-1b.noarch.rpm')
    def test_to_cpp(self):
        assert_equals(self.a.to_cpp(),'hello-1.0-1b/noarch:b\n')
    def test_to_xml(self):
        assert_equals(self.a.to_xml(),'   <package><name>hello</name><v>1.0</v><r>1b/noarch</r><options>b</options></package>\n')
    def test_to_summary(self):
        assert_equals(self.a.to_summary(),'hello:\n version=1.0-1b\n    arch=noarch\n')
    def test_to_eval(self):
        assert_equals(self.a.to_eval(),'+hello-1.0-1b/noarch:b')
