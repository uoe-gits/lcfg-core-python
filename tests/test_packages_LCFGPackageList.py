import lcfg.packages
import lcfg.common

import nose
from nose.tools import assert_equals, assert_true, assert_false, assert_raises, raises

def test_create_pkglist():
    a = lcfg.packages.LCFGPackageList()
def test_merge_rules():
    a = lcfg.packages.LCFGPackageList()
    assert_equals(a.merge_rules,lcfg.common.LCFG_MERGE_RULE_NONE)
def test_merge_rules_set():
    a = lcfg.packages.LCFGPackageList()
    a.merge_rules = lcfg.common.LCFG_MERGE_RULE_SQUASH_IDENTICAL
    assert_equals(a.merge_rules,lcfg.common.LCFG_MERGE_RULE_SQUASH_IDENTICAL)
def test_pkglist_size():
    a = lcfg.packages.LCFGPackageList()
    assert_equals(len(a),0)

def test_pkglist_iterator():
    packages = ['hello-1.0-1b',
                '+world-2.0-10/noarch',
                '+blubber-2.0-10/noarch',
                'other-2.0-3',
                'yet_another-2.2-10']
    a = lcfg.packages.LCFGPackageList()
    for p in packages:
        a.add(lcfg.packages.LCFGPackage(p))
    assert_equals(len(a),len(packages))
    for p,pn in zip(a,packages):
        assert_equals(p.__repr__(),pn)
    
class TestPkgListBase:
    def setup(self):
        self.pkglist = lcfg.packages.LCFGPackageList()
        self.a = lcfg.packages.LCFGPackage('hello-1.0-1b')
        self.b = lcfg.packages.LCFGPackage('+world-2.0-10/noarch')
    def teardown(self):
        del self.pkglist
        del self.a
        del self.b
   
class TestPkgList(TestPkgListBase):
    def test_add_inline(self):
        self.pkglist.add(lcfg.packages.LCFGPackage('+blubber-2.0-10/noarch'))
        assert_equals(len(self.pkglist),1)
        
    def test_add_length(self):
        self.pkglist.add(self.a)
        assert_equals(len(self.pkglist),1)
        self.pkglist.add(self.b)
        assert_equals(len(self.pkglist),2)

class TestPkgListPackages(TestPkgListBase):
    def setup(self):
        super().setup()
        self.pkglist.add(self.a)
        self.pkglist.add(self.b)
        
    def test_add_length(self):
        assert_equals(len(self.pkglist),2)

    def test_contains(self):
        assert_true(('world','noarch') in self.pkglist)
        assert_true(('hello','') in self.pkglist)
        assert_false(('world','not_here') in self.pkglist)

    def test_access_true(self):
        p = self.pkglist[('world','noarch')]
        assert_equals(hash(p),hash(self.b))
    @raises(KeyError)
    def test_access_false(self):
        p = self.pkglist[('world','not_here')]

    def test_merge_list(self):
        other = lcfg.packages.LCFGPackageList()
        other.add(lcfg.packages.LCFGPackage('other-2.0-3'))
        other.add(lcfg.packages.LCFGPackage('yet_another-2.2-10'))

        self.pkglist.update(other)

        assert_equals(len(self.pkglist),4)
        assert_true(('yet_another','') in self.pkglist)

    def test_add_again(self):
        assert_equals(len(self.pkglist),2)
        self.pkglist.add(self.a)
        assert_equals(len(self.pkglist),2)

    def test_add_again_inline(self):
        raise nose.SkipTest
        assert_equals(len(self.pkglist),2)
        self.pkglist.add(lcfg.packages.LCFGPackage('hello-1.0-1b'))
        assert_equals(len(self.pkglist),2)
