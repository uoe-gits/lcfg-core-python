lcfg-core-python
================
Python wrapper for the core [LCFG](https://www.lcfg.org) C library.

Installation
------------
Apart from the LCFG core C library including headers you need a basic python stack and the [pkgconfig](https://github.com/matze/pkgconfig) package installed. The wrapper is generated using [swig](http://www.swig.org/). If you want to run the tests you also need to install [nose](https://nose.readthedocs.io/en/latest/).

The python package is built using
```
python3 setup.py build
```

You can run the test suite using
```
python3 setup.py nosetests
```
